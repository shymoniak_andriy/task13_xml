package com.shymoniak.model;

import com.shymoniak.model.entity.Candy;
import com.shymoniak.model.parser.SAXHandler;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.List;

// TODO: 18.05.2019  Створити файл XML і відповідну йому схему XSD.
/* TODO: 18.05.2019  При розробленні XSD використовувати прості та комплексі типи, переліки,
                     шаблони та граничні значення.

 */
// TODO: 18.05.2019 Написати Java-клас, що відповідає поданому описанню. 
/* TODO: 18.05.2019  Створити Java-програму для опрацювання XML-документа та ініціалізації колекції
                     об’єктів інформацією з XML-файлу. Для опрацювання документа використовувати
                     SAX, DOM та StAX парсери.
 */
// TODO: 18.05.2019 Перевірити коректність XML-документа з використанням XSD.
/* TODO: 18.05.2019  Написати метод, що здійснює перетворення розробленого XML-документа за допомогою
                     XSL у формат HTML (відсортувавши об’єкти по одному з параметрів) та у формат XML
                     зі зміненим кореневим елементом.
 */
// TODO: 18.05.2019 Для сортування об’єктів використовувати інтерфейс Comparator.

/**
 * ТЕМА
 * Цукерки.
 *  Name – назва цукерки.
 *  Energy– калорійність (ккал).
 *  Type – тип цукерки (карамель, ірис, шоколадна [з начинкою або без]).
 *  Ingredient (повинно бути кілька) – інгредієнти: вода, цукор (в мг), фруктоза
 * (в мг), тип шоколаду (для шоколадних), ванілін (в мг)
 *  Value – харчова цінність: білки (в гр.), жири (в гр.) і вуглеводи (в гр.).
 *  Production – підприємство-виробник.
 * Кореневий елемент назвати Candies.
 */
public class Task {

    public static final String FILE_PATH = "D:" + File.separator + "EPAM" + File.separator + "homework16(XML)"
            + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator;

    public void createIfNotExistFile(File file) {
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void printList(List<Candy> candies) {
        for (Candy candy : candies) {
            System.out.println(candies);
        }
    }

    public void writeInXML(List<Candy> candies) {
        try (
                XMLEncoder encoder = new XMLEncoder(
                        new BufferedOutputStream(
                                new FileOutputStream(MainModel.fileXML.getPath())));
        ) {
            for (Candy candy: candies){
                encoder.writeObject(candy);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void parserSAX(File file) {
        SAXParserFactory parseF = SAXParserFactory.newInstance();
        SAXParser saxParser = null;
        SAXHandler handler = new SAXHandler();
        try {
            saxParser = parseF.newSAXParser();
            saxParser.parse(file, handler);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
