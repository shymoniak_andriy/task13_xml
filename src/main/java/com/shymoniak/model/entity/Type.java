package com.shymoniak.model.entity;

import lombok.ToString;

/**
 * Type – тип цукерки (карамель, ірис, шоколадна [з начинкою або без]).
 */
@ToString
public enum Type {
    CARAMEL("CARAMEL"), IRIS("IRIS"), CHOCOLATE("CHOCOLATE"),
    LOLLIPOP("LOLLIPOP"), NOUGAT("NOUGAT"), GLAZE("GLAZE");

    private String prodType;

    Type(String prodType) {
        this.prodType = prodType;
    }
}
