package com.shymoniak.model.entity;

import lombok.*;

import java.util.List;

/**
 *              ТЕМА
 * Цукерки.
 *  Name – назва цукерки.
 *  Energy– калорійність (ккал).
 *  Type – тип цукерки (карамель, ірис, шоколадна [з начинкою або без]).
 *  Ingredient (повинно бути кілька) – інгредієнти: вода, цукор (в мг), фруктоза
 * (в мг), тип шоколаду (для шоколадних), ванілін (в мг)
 *  Value – харчова цінність: білки (в гр.), жири (в гр.) і вуглеводи (в гр.).
 *  Production – підприємство-виробник.
 * Кореневий елемент назвати Candies.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Candy {
    private String name;
    private int calories;
    private Type type;
    private List<Ingredient> ingredients;
    private Production production;
}
