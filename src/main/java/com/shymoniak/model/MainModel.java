package com.shymoniak.model;

import com.shymoniak.model.entity.Candy;
import com.shymoniak.model.entity.Ingredient;
import com.shymoniak.model.entity.Production;
import com.shymoniak.model.entity.Type;
import com.shymoniak.model.parser.SAXParserUser;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainModel {

    Task task = new Task();

    public static final File fileXML = new File(Task.FILE_PATH + "candiesXML.xml");
    public static final File fileXSD = new File(Task.FILE_PATH + "candiesXSD.xml");

    public void printTask1() {
        task.createIfNotExistFile(fileXML);
        task.createIfNotExistFile(fileXSD);
//        task.writeInXML(getCandyList());
//        printList(SAXParserUser.parseCandies(fileXML, fileXSD), "SAX");
        task.printList(SAXParserUser.parseCandies(fileXML,fileXSD));
    }

    private static boolean checkIfXSD(File xsd) {
        return ExtensionChecker.isXSD(xsd);
    }

    private static boolean checkIfXML(File xml) {
        return ExtensionChecker.isXML(xml);
    }

//    private static void printList(List<Candy> candies, String parserName) {
//        System.out.println(parserName);
//        for (Candy candy : candies) {
//            System.out.println(candy);
//        }
//    }


    public List<Candy> getCandyList() {
        List<Candy> candies = new ArrayList<>();
        List<Ingredient> ingredients = new ArrayList<>();
        candies.add(new Candy("Шоколапки", 463, Type.NOUGAT, ingredients, Production.ROSHEN));
        candies.add(new Candy("Ромашка", 440, Type.GLAZE, ingredients, Production.ROSHEN));
        candies.add(new Candy("Монблан", 580, Type.CHOCOLATE, ingredients, Production.ROSHEN));
        candies.add(new Candy("Шоколад з манго", 600, Type.CHOCOLATE, ingredients, Production.AVK));
        candies.add(new Candy("Шоколад з горіхом", 600, Type.CHOCOLATE, ingredients, Production.AVK));
        candies.add(new Candy("Nutty Crunch Surprise", 550, Type.CHOCOLATE, ingredients, Production.WILLY_WONKA));
        candies.add(new Candy("Chilly Chocolate Creme", 503, Type.CHOCOLATE, ingredients, Production.WILLY_WONKA));
        candies.add(new Candy("Triple Dazzle Caramel", 550, Type.CHOCOLATE, ingredients, Production.WILLY_WONKA));
        candies.add(new Candy("Whipple Scrumptious Fudgemallow Delight", 580, Type.CHOCOLATE, ingredients, Production.WILLY_WONKA));
        candies.add(new Candy("Whipple Scrumptious Fudgemallow Delight", 565, Type.CHOCOLATE, ingredients, Production.WILLY_WONKA));
        candies.add(new Candy("Червоний мак", 540, Type.CHOCOLATE, ingredients, Production.SVITOCH));
        candies.add(new Candy("Зоряне сяйво", 490, Type.CHOCOLATE, ingredients, Production.SVITOCH));
        return candies;
    }
}
