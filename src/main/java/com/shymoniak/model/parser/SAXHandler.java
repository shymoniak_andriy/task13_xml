package com.shymoniak.model.parser;

import com.shymoniak.model.entity.Candy;
import com.shymoniak.model.entity.Ingredient;
import com.shymoniak.model.entity.Production;
import com.shymoniak.model.entity.Type;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private List<Candy> candies = null;
    private Candy candy = null;
    private Ingredient ingredient = null;
    private List<Ingredient> ingredients = null;

    private boolean bName = false;
    private boolean bCalories = false;
    private boolean bType = false;
    private boolean bIngredients = false;
    private boolean bIngredient = false;
    private boolean bProduction = false;
    private boolean bIngredientName = false;
    private boolean bQuality = false;

    public List<Candy> getCandyList() {
        return this.candies;
    }

    @Override
    public void startDocument() {
        System.out.println("XML parsing started (SAX method)...");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("candy")) {
            candy = new Candy();
        } else if (qName.equals("name")) {
            bName = true;
        } else if (qName.equals("calories")) {
            bCalories = true;
        } else if (qName.equals("type")) {
            bType = true;
        } else if (qName.equals("ingredients")) {
            bIngredients = true;
        } else if (qName.equals("production")) {
            bProduction = true;
        } else if (qName.equals("ingredientName")) {
            bIngredientName = true;
        } else if (qName.equals("quality")) {
            bQuality = true;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("candy")) {
            candies.add(candy);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (bName) {
            candy.setName(new String(ch, start, length));
            bName = false;
        } else if (bCalories) {
            candy.setCalories(Integer.parseInt(new String(ch, start, length)));
            bCalories = false;
        } else if (bType) {
            String type = new String(ch, start, length);
            if (type.equals(Type.CHOCOLATE.toString())) {
                candy.setType(Type.CHOCOLATE);
                bType = false;
            } else if (type.equals(Type.GLAZE.toString())) {
                candy.setType(Type.GLAZE);
                bType = false;
            } else if (type.equals(Type.NOUGAT.toString())) {
                candy.setType(Type.NOUGAT);
                bType = false;
            } else if (type.equals(Type.CARAMEL.toString())) {
                candy.setType(Type.CARAMEL);
                bType = false;
            } else if (type.equals(Type.CARAMEL.toString())) {
                candy.setType(Type.CARAMEL);
                bType = false;
            } else if (type.equals(Type.IRIS.toString())) {
                candy.setType(Type.IRIS);
                bType = false;
            }
        } else if (bIngredients) {
            candy.setIngredients(new ArrayList<>());
            bIngredients = false;
        } else if (bIngredient) {
            ingredient = new Ingredient();
            bIngredient = false;
        } else if (bProduction) {
            String prod = new String(ch, start, length);
            if (prod.equals(Production.WILLY_WONKA.toString())) {
                candy.setProduction(Production.WILLY_WONKA);
                bProduction = false;
            } else if (prod.equals(Production.ROSHEN.toString())) {
                candy.setProduction(Production.ROSHEN);
                bProduction = false;
            } else if (prod.equals(Production.AVK.toString())) {
                candy.setProduction(Production.AVK);
                bProduction = false;
            } else if (prod.equals(Production.SVITOCH.toString())) {
                candy.setProduction(Production.SVITOCH);
                bProduction = false;
            }
        } else if (bIngredientName) {
            ingredient.setIngredientName(new String(ch, start, length));
            bIngredientName = false;
        } else if (bQuality) {
            ingredient.setQuality(Integer.parseInt(new String(ch, start, length)));
            bQuality = false;
        } else if (bIngredientName) {
            ingredient.setIngredientName(new String(ch, start, length));
            bIngredientName = false;
        }
    }
}
